#!/bin/bash

ln -sf /usr/share/zoneinfo/America/Denver /etc/localtime
hwclock --systohc
sed -i '177s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=de_CH-latin1" >> /etc/vconsole.conf
echo "jose" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 jose.localdomain jose" >> /etc/hosts
echo root:password | chpasswd

pacman -S base-devel xdg-utils firewall

# TODO: Add personal packages using script

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups.service
systemctl enable sshd
systemctl enable firewalld
# If installing on a laptop uncomment the following
#systemctl enable acpid

useradd -m jose
echo jose:password | chpasswd
# EDITOR=vim visudo
# Uncomment first wheel line
usermod -aG wheel jose
#usermod -aG libvirt jose

#echo "jose ALL=(ALL) ALL" >> /etc/sudoers.d/jose

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
