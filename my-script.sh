#!/bin/bash

COMPUTER_NAME='UsingZornsLemma'
USER_NAME='jose'

# Setup timezone
ln -sf /usr/share/zoneinfo/America/Denver /etc/localtime
hwclock --systohc

# Setup locale
sed -i '177s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf

# Setup hostname
echo "$COMPUTER_NAME" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 $COMPUTER_NAME.localdomain $COMPUTER_NAME" >> /etc/hosts

# Setup root password
# TODO: Double check if this works fine
echo root:password | chpasswd

# Setup packages
pacman -S base-devel xdg-utils firewall
# TODO: Add personal packages using script
# Uncomment the following 
# if system uses nvidia hardware
# pacman -S nvidia nvidia-utils

# Setup GRUB
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
grub-mkconfig -o /boot/grub/grub.cfg

# Enable services
systemctl enable NetworkManager
systemctl enable bluetooth
systemctl enable cups
systemctl enable sshd
systemctl enable firewalld
# Because we are using KDE, enable the following
systemctl enable sddm
# If installing on a laptop uncomment the following
#systemctl enable acpid

# Add new user
useradd -m $USER_NAME
# TODO: Double check if this works fine
echo $USER_NAME:password | chpasswd
# Perform the following manually
# EDITOR=vim visudo
# Uncomment first wheel line
usermod -aG wheel $USER_NAME

printf "\e[1;32mDone! Type exit, umount -a and reboot.\e[0m"
